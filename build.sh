#! /bin/sh -

usage () {
  cat <<__EOF__
  ${0##*/} [HUGO_VERSION=<VER>] [SASS_VERSION=<VER>]
__EOF__
}

fatal () {
  echo "$*" 1>&2
  exit 64
}

gh_latest_release () (
  wget -q -O - --header 'Accept: application/vnd.github.v3+json' \
    "https://api.github.com/repos/${1}/releases?per_page=1" \
  | fgrep '"tag_name":' | sed 's/^[^:]*: *//;s/,$//;s/"//g'
)


while [ "$#" -gt 0 ]; do
  case "$1" in
    -*) ;;
    *=*)
      eval "${1%%=*}=${1#*=}"
      ;;
    *)
      eval "${1}=true"
      ;;
  esac
  shift
done

: ${HUGO_VERSION:=$(gh_latest_release gohugoio/hugo | sed 's/^v//')}
: ${SASS_VERSION:=$(gh_latest_release sass/dart-sass-embedded)}

gh='https://github.com'

repo='gohugoio/hugo' tag="v${HUGO_VERSION}"
hugo_base="${gh}/${repo}/releases/download/${tag}"
hugo_file="hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz"
hugo_sums="hugo_${HUGO_VERSION}_checksums.txt"

repo='sass/dart-sass-embedded' tag="${SASS_VERSION}"
sass_base="${gh}/${repo}/releases/download/${tag}"
sass_file="sass_embedded-${SASS_VERSION}-linux-x64.tar.gz"

set -e
[ -d tmp ] || mkdir tmp
trap "rm -r ${PWD:-.}/tmp" 0 15

cd tmp
curl -s -gL \
  -O "${hugo_base}/${hugo_file}" \
  -O "${hugo_base}/${hugo_sums}" \
  -O "${sass_base}/${sass_file}";

fgrep "${hugo_file}" "${hugo_sums}" | sha256sum -c || fatal "checksum failed"
tar xzf "${hugo_file}"
tar xzf "${sass_file}"

cd ..
[ -d bin ] || mkdir bin
[ -d doc ] || mkdir doc
[ -d doc/hugo ] || mkdir doc/hugo
[ -d doc/sass_embedded ] || mkdir doc/sass_embedded

mv tmp/hugo tmp/sass_embedded/dart-sass-embedded  bin/
mv tmp/LICENSE tmp/README.md doc/hugo/
mv tmp/sass_embedded/src/LICENSE  doc/sass_embedded/

exit 0
