#! /bin/sh -

sucmd=gosu
hugo=/opt/hugo/bin/hugo

show_license () {
	echo "[Hugo]"
	cat /opt/hugo/doc/hugo/LICENSE
	echo ""
	echo "[Dart Sass Embedded]"
	cat /opt/hugo/doc/sass_embedded/LICENSE
	return 0
}

: ${HUGO_BIND:=0.0.0.0} ${HUGO_PORT:=1313}
while [ "$#" -gt 0 ]; do
	case "$1" in
		-*)
		global_options="${global_options}${global_options:+ }$1"
			;;
		*)
			break
			;;
	esac
	shift
done

command="$1"
case "${command}" in
	build)
		shift
		set - "$@"
		;;
	license)
		show_license && exit 0
		;;
	serve|server)
		shift
		set - "${command}" "--bind=${HUGO_BIND}" "--port=${HUGO_PORT}"  "$@"
		;;
esac

set - ${hugo} ${global_options} "$@";
if [ -n "${HUGO_UID}" ]; then
    set - ${sucmd} "${HUGO_UID}" "$@";
fi

"$@" &
pid="$!"

trap "kill -HUP  ${pid};"  1
trap "kill -INT  ${pid};"  2
trap "kill -TERM ${pid};" 15

wait; exit $?;

