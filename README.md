# Hugo Extended Edition

Alpine based image to run Hugo static site generator.


## Usage

```sh
# Example: show license
docker run -it --rm eeldog/hugo-extended license

# Example: build your site
docker run -it --rm -v "${PWD}:/work" -e HUGO_UID="$(id -u)" eeldog/hugo-extended --cleanDestinationDir

# Example: hugo server
docker run -d -it --rm -p 127.0.0.1:1313:1313 -v "${PWD}:/work" eeldog/hugo-extended server --disableFastRender
xdg-open http://localhost:1313/
```

### Docker Compose

With the sample `docker-compose.yaml`:
```yaml
version: '3'

services:
  hugo:
    image: eeldog/hugo-extended:${HUGO_VERSION:-latest}
    build: .
    restart: "no"
    environment:
      - HUGO_BIND=${HUGO_BIND:-0.0.0.0}
      - HUGO_PORT=${HUGO_PORT:-1313}
      - HUGO_UID=${HUGO_UID:-}
    ports:
      - "127.0.0.1:${HUGO_PORT:-1313}:${HUGO_PORT:-1313}"
    volumes:
      - "${PWD}:/work"
    command: []
```

you can do:
```sh
# Example: build your site
echo "HUGO_UID=$(id -u)" >> .env
docker compose run hugo --cleanDestinationDir

# Example: hugo server
docker compose run --service-ports hugo server --disableFastRender
xdg-open http://localhost:1313/
```
Note 1: `docker compose run` ignores `ports:` element.  You need 
`--service-ports` option, or `docker compose up` command.

Note 2: `hugo server` appends the port number to base URL,
so it should be same between the container and the host.

## Environment Variables

* `HUGO_UID`: Run as user specified by uid
* `HUGO_BIND`: Bind address in `hugo server` mode (which should be '0.0.0.0'.)
* `HUGO_PORT`: Bind port in `hugo server` mode (default: 1313.)

