FROM alpine AS factory
  ARG HUGO_VERSION
  ARG SASS_VERSION
  WORKDIR /build
  COPY build.sh .
  RUN set -e && apk add --no-cache -t .deps ca-certificates curl \
	;
  RUN set -e \
    && /bin/sh ./build.sh HUGO_VERSION=${HUGO_VERSION} SASS_VERSION=${SASS_VERSION} \
    && apk del .deps && rm build.sh \
    ;


FROM debian:stable-slim AS product
  ARG HUGO_VERSION
  ARG HUGO_BIND
  ARG HUGO_PORT
  ARG HUGO_WORKDIR=/work

  ENV HUGO_BIND=${HUGO_BIND:-0.0.0.0} HUGO_PORT=${HUGO_PORT:-1313}
  ENV PATH=/opt/hugo/bin:${PATH}

  WORKDIR /opt/hugo
  COPY init.sh .
  COPY --from=factory /build .
  RUN set -e && apt-get update && apt-get install -y gosu \
  	&& gosu nobody true \
  	&& rm -rf /var/lib/apt/lists/* \
	;

  WORKDIR ${HUGO_WORKDIR}
  ENTRYPOINT [ "/opt/hugo/init.sh" ]
  EXPOSE ${HUGO_PORT}/tcp
  VOLUME ${HUGO_WORKDIR}

